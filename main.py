from logger import *

@log
def mul(first_arg, second_arg):
    return first_arg * second_arg

@log
def add(first_arg, second_arg):
    return first_arg + second_arg

@log
def sub(first_arg, second_arg):
    return first_arg - second_arg

@log
def div(first_arg, second_arg):
    return first_arg / second_arg

@log
def mod(first_arg, second_arg):
    return first_arg % second_arg
